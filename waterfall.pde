/*
 * Waterfall Animation by Sean Castillo
 * Reference: Sine Wave code from example on Processing.org by Daniel Shiffman
 */

float speed = 5;
int[] backgroundColor = {0, 154, 105}; //rgb values for color of background... or in this case, the water
int counter = 0;


//walls
float[] line0 = {400, 0, 400, 500};
float[] line1 = {500, 0, 500, 500};
float[][] lineArrayWall = {line0, line1};
//right lines
float[] line2 = {400, 0, 500, 0};
float[] line3 = {400, -200, 500, -20};
float[] line4 = {400, -400, 500, -40};
float[] line5 = {400, -600, 500, -60};
float[] line6 = {400, -800, 500, -80};
float[][] lineArrayRight = {line2, line3, line4, line5, line6};
//left lines
float[] line7 = {500, 0, 400, 0};
float[] line8 = {500, -200, 400, -20};
float[] line9 = {500, -400, 400, -40};
float[] line10 = {500, -600, 400, -60};
float[] line11 = {500, -800, 400, -80};
float[][] lineArrayLeft = {line7, line8, line9, line10, line11};

float[] coverRectangle = {0, 450, 900, 450};

//other line
float[] otherLine1 = {0, 400, 900, 400};
float[] otherLine2 = {0, 380, 900, 380};


//ellipse
float[] ellipse1 = {400, 400, 20, 20};
boolean ellipseSwitch = false;
int ellipseCounter = 0;


//waterfall bottom line wall
float[] waterfallBottomWall0 = {0, 900, 400, 445};
float[] waterfallBottomWall1 = {500, 445, 900, 900};
float[][] waterfallBottomWallArray = {waterfallBottomWall0, waterfallBottomWall1};

//ripples at bottom of waterfall
//arc syntax: x-coord, y-coord, width, height, starting angle (radians), stopping angle (radians)
float[] rippleArc0 = {450, 450, 100, 100, 0, PI};
float[] rippleArc1 = {450, 430, 100, 100, 0, PI};
float[] rippleArc2 = {450, 410, 100, 100, 0, PI};
float[] rippleArc3 = {450, 390, 100, 100, 0, PI};
float[] rippleArc4 = {450, 370, 100, 100, 0, PI};
float[] rippleArc5 = {450, 340, 100, 100, 0, PI};
float[] rippleArc6 = {450, 320, 100, 100, 0, PI};
float[] rippleArc7 = {450, 300, 100, 100, 0, PI};
float[] rippleArc8 = {450, 280, 100, 100, 0, PI};
float[] rippleArc9 = {450, 260, 100, 100, 0, PI};
float[] rippleArc10 = {450, 220, 100, 100, 0, PI};
float[] rippleArc11 = {450, 200, 100, 100, 0, PI};
float[] rippleArc12 = {450, 180, 100, 100, 0, PI};
float[] rippleArc13 = {450, 160, 100, 100, 0, PI};
float[] rippleArc14 = {450, 140, 100, 100, 0, PI};
float[] rippleArc15 = {450, 120, 100, 100, 0, PI};
float[] rippleArc16 = {450, 100, 100, 100, 0, PI};
float[] rippleArc17 = {450, 80, 100, 100, 0, PI};
float[] rippleArc18 = {450, 60, 100, 100, 0, PI};
float[] rippleArc19 = {450, 40, 100, 100, 0, PI};
float[] rippleArc20 = {450, 20, 100, 100, 0, PI};
float[][] rippleArcArray = {rippleArc0, rippleArc1, rippleArc2, rippleArc3, rippleArc4, rippleArc5, 
  rippleArc6, rippleArc7, rippleArc8, rippleArc9, rippleArc10, rippleArc11, 
  rippleArc12, rippleArc13, rippleArc14, rippleArc15, rippleArc16, rippleArc17, 
  rippleArc18, rippleArc19, rippleArc20};
int rippleCounter = 0;

//cover-up rectangle so that the piece is viewed when things are properly spawning
float[] startCoverRec = {0, 0, 900, 900};

//rectangles to cover up the sides
float[] staticCoverRect0 = {0, 0, 395, 445};
float[] staticCoverRect1 = {503, 0, 395, 445};
float[][] staticCoverRectArray = {staticCoverRect0, staticCoverRect1};
//triangles to cover up the sides
float[] staticCoverTri0 = {0, 445, 400, 445, 0, 900};
float[] staticCoverTri1 = {500, 445, 900, 445, 900, 900};
float[][] staticCoverTriArray = {staticCoverTri0, staticCoverTri1};

void setup()
{
  frameRate(60);
  size(900, 900);
}

void draw()
{
  background(backgroundColor[0], backgroundColor[1], backgroundColor[2]);
  moveCoverRect();
  moveLine();
  moveOtherLine();
  moveRipple();
  stroke(0); //Processing uses the most recently specified stroke/fill/etc.,
  //reading code from top to bottom.
  fill(0, 0, 0); //makes rectangles black

  strokeWeight(8);
  for (int i=0; i<lineArrayWall.length; i++)
  { 
    line(lineArrayWall[i][0], lineArrayWall[i][1], lineArrayWall[i][2], lineArrayWall[i][3]);
  }
  for (int i=0; i<lineArrayRight.length; i++)
  {
    line(lineArrayRight[i][0], lineArrayRight[i][1], lineArrayRight[i][2], lineArrayRight[i][3]);
  }
  for (int i=0; i<lineArrayLeft.length; i++)
  {
    line(lineArrayLeft[i][0], lineArrayLeft[i][1], lineArrayLeft[i][2], lineArrayLeft[i][3]);
  }

  stroke(backgroundColor[0], backgroundColor[1], backgroundColor[2]);
  fill(backgroundColor[0], backgroundColor[1], backgroundColor[2]);
  rect(coverRectangle[0], coverRectangle[1], coverRectangle[2], coverRectangle[3]);

  stroke(0);
  for (int i=0; i<waterfallBottomWallArray.length; i++)
  {
    line(waterfallBottomWallArray[i][0], waterfallBottomWallArray[i][1], waterfallBottomWallArray[i][2], waterfallBottomWallArray[i][3]);
  }

  strokeWeight(5);
  noFill();
  for (int i=0; i< rippleArcArray.length; i++)
  {
    arc(rippleArcArray[i][0], rippleArcArray[i][1], rippleArcArray[i][2], rippleArcArray[i][3], rippleArcArray[i][4], rippleArcArray[i][5]);
  }

  fill(0);
  stroke(0);
  rect(startCoverRec[0], startCoverRec[1], startCoverRec[2], startCoverRec[3]);

  for (int i=0; i<staticCoverRectArray.length; i++)
  {
    rect(staticCoverRectArray[i][0], staticCoverRectArray[i][1], staticCoverRectArray[i][2], staticCoverRectArray[i][3]);
  }
  noStroke();
  for (int i=0; i<staticCoverTriArray.length; i++)
  {
    triangle(staticCoverTriArray[i][0], staticCoverTriArray[i][1], staticCoverTriArray[i][2], staticCoverTriArray[i][3], staticCoverTriArray[i][4], staticCoverTriArray[i][5]);
  }
}

void moveLine()
{
  for (int i=0; i<lineArrayRight.length; i++)
  {
    lineArrayRight[i][1] += speed * 2.5;
    lineArrayRight[i][3] += speed;

    if (lineArrayRight[i][1] > 900)
    {
      lineArrayRight[i][1] = -100;
      lineArrayRight[i][3] = -100;
    }
  }
  for (int i=0; i<lineArrayLeft.length; i++)
  {
    lineArrayLeft[i][1] += speed * 2.5;
    lineArrayLeft[i][3] += speed;

    if (lineArrayLeft[i][1] > 900)
    {
      lineArrayLeft[i][1] = -100;
      lineArrayLeft[i][3] = -100;
    }
  }
}

void moveOtherLine()
{
  otherLine1[1] -= speed/3;
  otherLine1[3] -= speed/3;
  otherLine2[1] -= speed/3;
  otherLine2[3] -= speed/3;
  if (otherLine1[1] < -20)
  {
    otherLine1[1] = height;
    otherLine1[3] = height;
  }
  if (otherLine2[1] < -20)
  {
    otherLine2[1] = height;
    otherLine2[3] = height;
  }
}

void moveRipple()
{
  for (int i=0; i<rippleArcArray.length; i++)
  {
    rippleArcArray[i][1] += 1;
    rippleArcArray[i][2] += 1.75;
    if (rippleArcArray[i][1] == height)
    {
      rippleArcArray[i][1] = 450;
      rippleArcArray[i][2] = 100;
    }
  }
}

void moveCoverRect()
{
  startCoverRec[1] += 1.0;
}